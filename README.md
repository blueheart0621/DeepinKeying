## 1. DeepinKeying
本仓库存储的是 Deepin20 官方源的 GPG 验证文件，由于 Deepin20 官方源的 GPG Key 目前在 `keyserver.ubuntu.com` 等公钥服务器上找不到，故对于其他 Debian 系的 Linux 操作系统（如 Ubuntu）来说只能通过拷贝 Deepin 系统下的 GPG 验证文件来使用 Deepin20 官方源。
- 有需要的小伙伴可以下载该仓库，并将其中所有的 GPG 文件拷贝到系统的 `/etc/apt/trusted.gpg.d` 目录中。